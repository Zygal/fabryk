#ifndef _SINGLETON_H__
#define _SINGLETON_H__

#include <assert.h>

template <class Temp>
class Singleton{
public:

    static Temp* getInstance(){
        return Temp::getInstance();
    }
};

#endif // _SINGLETON_H__
