#ifndef __MAIN_H_
#define __MAIN_H_

#include <vulkan/vulkan.h>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include "packages/fabryk/fabryk.h"
#include "globalState.h"

class FabrykEngine {
public:

    /// @brief A method to run the program
    void run(void);

private:

    /// @brief A method to intialize the enviornment
    void init();

    /// @brief the main program loop
    void mainLoop();

    /// @brief cleanup of the vulkan space
    void cleanup();

    /// @brief Initialize Vulkan
    void initVulkan();

    /// @section Private instances
    Fabryk_Window window;
    Fabryk_Instance instance;
    Fabryk_Physical_Device physicalDevice;
    Fabryk_Logical_Device logicalDevice;
};

/// @todo break main into an "application.cpp and .h"
/// @brief Main app control loop
int main(void);

#endif  // __MAIN_H_
