#include "main.h"

void FabrykEngine::run(void) {
    init();
    mainLoop();
    cleanup();
}

void FabrykEngine::init() {
    window.init();
    initVulkan();
}

void FabrykEngine::mainLoop() {
    while (window.isOpen()) {
        window.pollEvents();
    }
}

void FabrykEngine::cleanup() {
    logicalDevice.close();
    instance.close();
    window.close();
}

void FabrykEngine::initVulkan() {

    instance.addDefaultsToRequiredValidationLayers();
    instance.addExtensions(window.getGlfwExtensions());
    instance.verifyExtensions();
    instance.init();

    physicalDevice.init(&instance);

    logicalDevice.init(&physicalDevice);
}

int main() {

    FabrykEngine app;

    try {
        app.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    /// @example Global State references and inherited pieces
     //GlobalStore::GSInstance()->GSInstance();

    return EXIT_SUCCESS;
}
