#include "headers.h"

class Fabryk_Window {
public:
	// void setBorders(int type) {
	// }

	std::vector<const char*> getGlfwExtensions() {
		const char ** glfwExtensions;
		uint32_t glfwExtensionCount = 0;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> glfwExtensions_v(glfwExtensions, glfwExtensions + glfwExtensionCount);
		return glfwExtensions_v;
	}

	void setName(std::string windowName) {
		this->windowName = windowName;
	}
	void setDimensions(int width, int height) {
		this->width = width;
		this->height = height;
	}
	void init() {
		setDefaults();
		glfwInit();

		//No OpenGL
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		window = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
	}
	bool isOpen() {
		return !glfwWindowShouldClose(window);
	}
	void pollEvents() {
		glfwPollEvents();
	}
	void close() {
		glfwDestroyWindow(window);
		glfwTerminate();
	}
private:
	GLFWwindow * window;

	std::string windowName;
	int width = 0;
	int height = 0;

	void setDefaults() {
		if (width == 0) {
			width = 800;
		}
		if (height == 0) {
			height = 600;
		}
		if (windowName.empty()) {
			windowName = "Fabrica Engine";
		}
	}

};