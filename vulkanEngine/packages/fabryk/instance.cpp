#include "headers.h"
#define FABRYK_NAME "fabryk"

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	} else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr) {
		func(instance, debugMessenger, pAllocator);
	}
}

class Fabryk_Instance {
public:
	void addExtensions(std::vector<const char*> extensions_new) {
		if (!extensions_new.empty()) {
			extensions.insert(std::end(extensions),
				std::begin(extensions_new),
				std::end(extensions_new));
		}
	}
	void addExtension(const char* extension_new) {
		if (extension_new != nullptr) {
			extensions.push_back(extension_new);
		}
	}
	void verifyExtensions() {
		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableExtensions.data());

		bool extensionFound;
		if (extensions.size() > 0) {
			for (const char* extensionName : extensions) {
				extensionFound = false;

				for (auto& extensionProperties : availableExtensions) {
					if (strcmp(extensionName, extensionProperties.extensionName) == 0) {
						extensionFound = true;
						break;
					}
				}

				if (!extensionFound) {
					break;
				}
			}
		} else {
			extensionFound = true;
		}

		if (!extensionFound) {
			throw std::runtime_error("Fabryk_Instance: One of more extensions are not supported!");
		}
	}
	void init() {
		if (enableValidationLayers) {
			initValidation();
		}

		initAppInfo();
		initInstanceCreateInfo();

		if(vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
			throw std::runtime_error("Fabryk_Instance: failed to create vulkan instance.");
		}

		initDebugMessenger();
	}
	void close() {
		if (enableValidationLayers) {
			closeValidation();
		}
		vkDestroyInstance(instance, nullptr);

	}

	VkInstance getVkInstance() {
		return instance;
	}

	//Validation
	void addRequiredValidationLayers(std::vector<char*> newRequiredLayers) {
		requiredValidationLayers.insert(std::end(requiredValidationLayers),
			std::begin(newRequiredLayers),
			std::end(newRequiredLayers));
	}
	void addPreferedValidationLayers(std::vector<char*> newPreferedLayers) {
		preferedValidationLayers.insert(std::end(preferedValidationLayers),
			std::begin(newPreferedLayers),
			std::end(newPreferedLayers));
	}
	void addDefaultsToRequiredValidationLayers() {
		addRequiredValidationLayers(defaultValidationLayers);
	}
	void addDefaultsToPreferedValidationLayers() {
		addPreferedValidationLayers(defaultValidationLayers);
	}
	std::vector<char*> getDefaultValidationLayers() {
		return defaultValidationLayers;
	}
	void setDefaultValidationLayers(std::vector<char*> newDefaults) {
		defaultValidationLayers = newDefaults;
	}
	std::vector<char*> getRequiredValidationLayersNotFound() {
		return requiredValidationLayersNotFound;
	}
	std::vector<char*> getPreferedValidationLayersNotFound() {
		return preferedValidationLayersNotFound;
	}
	bool isValidationEnabled() {
		return enableValidationLayers;
	}
private:
	//Instance
	VkInstance instance = {};
	VkApplicationInfo appInfo = {};
	VkInstanceCreateInfo createInfo = {};
	std::vector<const char*> extensions;

	//Validation
	#ifdef NDEBUG
		const bool enableValidationLayers = false;
	#else
		const bool enableValidationLayers = true;
	#endif

	std::vector<VkLayerProperties> availableValidationLayers;
	std::vector<char*> enabledValidationLayers;

	std::vector<char*> requiredValidationLayers;
	std::vector<char*> preferedValidationLayers;
	std::vector<char*> requiredValidationLayersNotFound;
	std::vector<char*> preferedValidationLayersNotFound;

	std::vector<char*> defaultValidationLayers = {
		(char *) "VK_LAYER_LUNARG_standard_validation"
	};

	//Debug Callback
	VkDebugUtilsMessengerEXT debugMessenger;

	//Instance
	void initAppInfo() {
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = FABRYK_NAME;
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = FABRYK_NAME;
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;
	}
	void initInstanceCreateInfo() {
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.enabledExtensionCount = extensions.size();
		createInfo.ppEnabledExtensionNames = extensions.data();

		if (enableValidationLayers) {
			createInfo.enabledLayerCount = enabledValidationLayers.size();
			createInfo.ppEnabledLayerNames = enabledValidationLayers.data();	
		} else {
			createInfo.enabledLayerCount = 0;
		}
	}
	//Validation
	void getAvailableValidationLayers() {
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		availableValidationLayers.resize(layerCount);

		vkEnumerateInstanceLayerProperties(&layerCount, availableValidationLayers.data());
	}
	bool getRequiredValidationLayers() {
		bool layerFound;
		if (requiredValidationLayers.size() > 0) {
			for (char* layerName : requiredValidationLayers) {
				layerFound = false;

				for (auto& layerProperties : availableValidationLayers) {
					if (strcmp(layerName, layerProperties.layerName) == 0) {
						layerFound = true;
						enabledValidationLayers.push_back(layerName);
						break;
					}
				}

				if (!layerFound) {
					requiredValidationLayersNotFound.push_back(layerName);
					break;
				}
			}
		} else {
			layerFound = true;
		}
		return layerFound;
	}
	void getPreferedValidationLayers() {
		bool layerFound;
		if (preferedValidationLayers.size() > 0) {
			for (char* layerName : preferedValidationLayers) {
				layerFound = false;
				for (auto& layerProperties : availableValidationLayers) {
					if (strcmp(layerName, layerProperties.layerName) == 0) {
						enabledValidationLayers.push_back(layerName);
						layerFound = true;
						break;
					}
				}

				if (!layerFound) {
					preferedValidationLayersNotFound.push_back(layerName);
				}

			}
		}
	}
	void setupValidationLayers() {
		if (!getRequiredValidationLayers()) {
			throw std::runtime_error("Error initializing required Validation Layers. One or more Layers are not available.");
		}
		getPreferedValidationLayers();
	}
	void initValidation() {
		getAvailableValidationLayers();

		if (requiredValidationLayers.size() == 0 && preferedValidationLayers.size() == 0) {
			addDefaultsToPreferedValidationLayers();
		}

		setupValidationLayers();

		addExtension(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}
	void closeValidation() {
		if (enableValidationLayers) {
			DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
		}
	}

	//Debug Callback

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData) {

		std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

		return VK_FALSE;
	}

	void initDebugMessenger() {
		if (!enableValidationLayers) return;

		VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = debugCallback;
		createInfo.pUserData = nullptr;

		if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
			throw std::runtime_error("failed to set up debug messenger!");
		}
	}

};