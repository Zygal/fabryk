#include "window.cpp"

#ifndef INSTANCE
#define INSTANCE
#include "instance.cpp"
#endif

#ifndef PHYSICAL_DEVICE
#define PHYSICAL_DEVICE
#include "physical_device.cpp"
#endif

#include "logical_device.cpp"