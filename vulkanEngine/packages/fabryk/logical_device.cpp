#include "headers.h"

#ifndef PHYSICAL_DEVICE
#define PHYSICAL_DEVICE
#include "physical_device.cpp"
#endif

class Fabryk_Logical_Device {
public:
	void init(Fabryk_Physical_Device * f_physicalDevice) {
		physicalDevice = f_physicalDevice->getVkPhysicalDevice();
		indices = f_physicalDevice->getQueueFamilyIndices();
		initQueueCreateInfo();
		initDeviceFeaturesInfo();
		initDeviceCreateInfo();

		if (vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create logical device.");
		}

		vkGetDeviceQueue(device, queueCreateInfo.queueFamilyIndex, 0, &graphicsQueue);
	}

	void close() {
		vkDestroyDevice(device, nullptr);
	}
private:
	VkPhysicalDevice physicalDevice;
	VkDevice device;

	QueueFamilyIndices indices;
	VkQueueFamilyProperties queueProperties;
	VkQueue graphicsQueue;

	VkDeviceQueueCreateInfo queueCreateInfo = {};
	VkPhysicalDeviceFeatures deviceFeatures = {};
	VkDeviceCreateInfo deviceCreateInfo = {};

	void initQueueCreateInfo() {
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = indices.graphicsFamily.value();
		queueCreateInfo.queueCount = 1;

		float queuePriority = 1.0f;
		queueCreateInfo.pQueuePriorities = &queuePriority;
	}
	void initDeviceFeaturesInfo() {}
	void initDeviceCreateInfo() {
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
		deviceCreateInfo.queueCreateInfoCount = 1;
		deviceCreateInfo.pEnabledFeatures = &deviceFeatures;
	}
};