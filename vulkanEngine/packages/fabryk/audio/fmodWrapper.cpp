#include "fmodWrapper.h"

Implementation* sgpImplementation = nullptr;

Implementation::Implementation() {
    mpStudioSystem = NULL;
    FModWrapper::CheckForErrors(FMOD::Studio::System::create(&mpStudioSystem));
    FModWrapper::CheckForErrors(mpStudioSystem->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL));

    mpSystem = NULL;
    FModWrapper::CheckForErrors(mpStudioSystem->getLowLevelSystem(&mpSystem));
}

Implementation::~Implementation() {
    FModWrapper::CheckForErrors(mpStudioSystem->unloadAll());
    FModWrapper::CheckForErrors(mpStudioSystem->release());
}

void Implementation::Update() {
    vector<ChannelMap::iterator> pStoppedChannels;
    for (auto it = mChannels.begin(), itEnd = mChannels.end(); it != itEnd; ++it)
    {
        bool bIsPlaying = false;
        it->second->isPlaying(&bIsPlaying);
        if (!bIsPlaying)
        {
             pStoppedChannels.push_back(it);
        }
    }
    for (auto &it : pStoppedChannels)
    {
         mChannels.erase(it);
    }
    FModWrapper::CheckForErrors(mpStudioSystem->update());
}

void FModWrapper::Init() {
    sgpImplementation = new Implementation;
}

void FModWrapper::Update() {
    sgpImplementation->Update();
}

void FModWrapper::LoadSound(const string &strSoundName, bool b3d = true, bool bLooping = false, bool bStream = false) {
        auto tFoundIt = sgpImplementation->mSounds.find(strSoundName);
    if (tFoundIt != sgpImplementation->mSounds.end())
        return;

    FMOD_MODE eMode = FMOD_DEFAULT;
    eMode |= b3d ? FMOD_3D : FMOD_2D;
    eMode |= bLooping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
    eMode |= bStream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;

    FMOD::Sound* pSound = nullptr;
    FModWrapper::CheckForErrors(sgpImplementation->mpSystem->createSound(strSoundName.c_str(), eMode, nullptr, &pSound));
    if (pSound){
        sgpImplementation->mSounds[strSoundName] = pSound;
    }
}

void FModWrapper::UnLoadSound(const std::string &strSoundName) {

    auto tFoundIt = sgpImplementation->mSounds.find(strSoundName);
    if (tFoundIt == sgpImplementation->mSounds.end())
        return;

    FModWrapper::CheckForErrors(tFoundIt->second->release());
    sgpImplementation->mSounds.erase(tFoundIt);
}

int FModWrapper::PlaySounds(const string &strSoundName, const Vector3 &vPosition, float fVolumedB) {
    int nChannelId = sgpImplementation->mnNextChannelId++;
    auto tFoundIt = sgpImplementation->mSounds.find(strSoundName);
    if (tFoundIt == sgpImplementation->mSounds.end())
    {
        LoadSound(strSoundName);
        tFoundIt = sgpImplementation->mSounds.find(strSoundName);
        if (tFoundIt == sgpImplementation->mSounds.end())
        {
            return nChannelId;
        }
    }
    FMOD::Channel* pChannel = nullptr;
    FModWrapper::CheckForErrors(sgpImplementation->mpSystem->playSound(tFoundIt->second, nullptr, true, &pChannel));
    if (pChannel)
    {
        FMOD_MODE currMode;
        tFoundIt->second->getMode(&currMode);
        if (currMode & FMOD_3D){
            FMOD_VECTOR position = VectorToFmod(vPosition);
            FModWrapper::CheckForErrors(pChannel->set3DAttributes(&position, nullptr));
        }
        FModWrapper::CheckForErrors(pChannel->setVolume(dbToVolume(fVolumedB)));
        FModWrapper::CheckForErrors(pChannel->setPaused(false));
        sgpImplementation->mChannels[nChannelId] = pChannel;
    }
    return nChannelId;
}

void FModWrapper::SetChannel3dPosition(int nChannelId, const Vector3 &vPosition)
{
    auto tFoundIt = sgpImplementation->mChannels.find(nChannelId);
    if (tFoundIt == sgpImplementation->mChannels.end())
        return;

    FMOD_VECTOR position = VectorToFmod(vPosition);
    FModWrapper::CheckForErrors(tFoundIt->second->set3DAttributes(&position, NULL));
}

void FModWrapper::SetChannelVolume(int nChannelId, float fVolumedB)
{
    auto tFoundIt = sgpImplementation->mChannels.find(nChannelId);
    if (tFoundIt == sgpImplementation->mChannels.end())
        return;

    FModWrapper::CheckForErrors(tFoundIt->second->setVolume(dbToVolume(fVolumedB)));
}

void FModWrapper::LoadBank(const std::string &strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags) {
    auto tFoundIt = sgpImplementation->mBanks.find(strBankName);
    if (tFoundIt != sgpImplementation->mBanks.end())
        return;
    FMOD::Studio::Bank* pBank;
    FModWrapper::CheckForErrors(sgpImplementation->mpStudioSystem->loadBankFile(strBankName.c_str(), flags, &pBank));
    if (pBank) {
        sgpImplementation->mBanks[strBankName] = pBank;
    }
}

void FModWrapper::LoadEvent(const std::string &strEventName) {
    auto tFoundit = sgpImplementation->mEvents.find(strEventName);
    if (tFoundit != sgpImplementation->mEvents.end())
        return;
    FMOD::Studio::EventDescription* pEventDescription = NULL;
    FModWrapper::CheckForErrors(sgpImplementation->mpStudioSystem->getEvent(strEventName.c_str(), &pEventDescription));
    if (pEventDescription){
        FMOD::Studio::EventInstance* pEventInstance = NULL;
        FModWrapper::CheckForErrors(pEventDescription->createInstance(&pEventInstance));
        if (pEventInstance){
            sgpImplementation->mEvents[strEventName] = pEventInstance;
        }
    }
}

void FModWrapper::PlayEvent(const string &strEventName) {
    auto tFoundit = sgpImplementation->mEvents.find(strEventName);
    if (tFoundit == sgpImplementation->mEvents.end()){
        LoadEvent(strEventName);
        tFoundit = sgpImplementation->mEvents.find(strEventName);
        if (tFoundit == sgpImplementation->mEvents.end())
            return;
    }
    tFoundit->second->start();
}

void FModWrapper::StopEvent(const string &strEventName, bool bImmediate) {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return;

    FMOD_STUDIO_STOP_MODE eMode;
    eMode = bImmediate ? FMOD_STUDIO_STOP_IMMEDIATE : FMOD_STUDIO_STOP_ALLOWFADEOUT;
    FModWrapper::CheckForErrors(tFoundIt->second->stop(eMode));
}

bool FModWrapper::IsEventPlaying(const string &strEventName) const {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return false;

    FMOD_STUDIO_PLAYBACK_STATE* state = NULL;
    if (tFoundIt->second->getPlaybackState(state) == FMOD_STUDIO_PLAYBACK_PLAYING) {
        return true;
    }
    return false;
}

void FModWrapper::GetEventParameter(const string &strEventName, const string &strParameterName, float* parameter) {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return;

    FMOD::Studio::ParameterInstance* pParameter = NULL;
    FModWrapper::CheckForErrors(tFoundIt->second->getParameter(strParameterName.c_str(), &pParameter));
    FModWrapper::CheckForErrors(pParameter->getValue(parameter));
}

void FModWrapper::SetEventParameter(const string &strEventName, const string &strParameterName, float fValue) {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return;

    FMOD::Studio::ParameterInstance* pParameter = NULL;
    FModWrapper::CheckForErrors(tFoundIt->second->getParameter(strParameterName.c_str(), &pParameter));
    FModWrapper::CheckForErrors(pParameter->setValue(fValue));
}

FMOD_VECTOR FModWrapper::VectorToFmod(const Vector3 &vPosition){
    FMOD_VECTOR fVec;
    fVec.x = vPosition.x;
    fVec.y = vPosition.y;
    fVec.z = vPosition.z;
    return fVec;
}

float FModWrapper::dbToVolume(float dB)
{
    return powf(10.0f, 0.05f * dB);
}

float FModWrapper::VolumeTodB(float volume)
{
    return 20.0f * log10f(volume);
}

int FModWrapper::CheckForErrors(FMOD_RESULT result) {
    if (result != FMOD_OK){
        cout << "FMOD ERROR " << result << endl;
        return 1;
    }
    // cout << "FMOD all good" << endl;
    return 0;
}

void FModWrapper::Shutdown() {
    delete sgpImplementation;
}

