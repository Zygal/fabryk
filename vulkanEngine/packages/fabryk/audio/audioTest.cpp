#include "audioTest.h"
//#include "fmodWrapper.h"
#include <time.h>
#include <stdio.h>

// Make a timer for the simulated draw-calls
clock_t start = 0;
double duration = 0;
uint8_t timerFlag = 0;
bool first = false;

int main(void) {

    FModWrapper audioInstance;

    if(!first) {
      audioInstance.init();
      first = true;
    }

    start = clock();
    
    // While the timer is not 1/60th of a second
    while(!timerFlag){
      duration = (clock() - start)/((double)((CLOCKS_PER_SEC)/1000));
      // Every 17ms call the ball
      if(duration = 17)
	timerFlag = true;
    }

    // Update the audio instance
    audioInstance.update();
    
}
