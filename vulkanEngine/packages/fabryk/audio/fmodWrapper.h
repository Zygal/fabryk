
#ifndef _FMODWRAPPER_H__
#define _FMODWRAPPER_H__

#include "fmod/api/core/inc/fmod.hpp"
#include "fmod/api/studio/inc/fmod_studio.hpp"
#include <string>
#include <map>
#include <vector>
#include <math.h>
#include <iostream>

/// @note saving some std's ;)
using namespace std;

/// @struct for the implementation API
struct Implementation {
    Implementation();
    ~Implementation();

    void Update();

    FMOD::Studio::System* mpStudioSystem;
    FMOD::System* mpSystem;

    int mnNextChannelId;

    typedef map<string, FMOD::Sound*> SoundMap;
    typedef map<int, FMOD::Channel*> ChannelMap;
    typedef map<string, FMOD::Studio::EventInstance*> EventMap;
    typedef map<string, FMOD::Studio::Bank*> BankMap;

    BankMap mBanks;
    EventMap mEvents;
    SoundMap mSounds;
    ChannelMap mChannels;
};

struct AudioVector {
    float xComponent;
    float yComponent;
    float zComponent;
};

class FModWrapper
{
public:
    /// @todo Define public wrapper functions... May not even need them if the mamager
    /// inherits the wrapper

    /// @brief initialize the wrapper
    static void init();

    /// @brief update the wrapper instance
    static void update();

    /// @brief close the wrapper instance
    static void close();

    static int CheckForErrors(FMOD_RESULT result);

    /// @brief load soundbank
    void LoadBank(const string &strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags);

    /// @brief load audio event
    void LoadEvent(const string &strEventName);

    void LoadSound(const string &strSoundName, bool b3d = true, bool bLooping = false, bool bStream = false);

    void UnLoadSound(const string &strSoundName);

    void Set3dListenerAndOrientation(const Vector3 &vPos = Vector3{ 0, 0, 0 }, float fVolumedB = 0.0f);

    void PlaySound(const string &strSoundName, const Vector3 &vPos = Vector3{ 0, 0, 0 }, float fVolumedB = 0.0f);

    void PlayEvent(const string &strEventName);

    void StopChannel(int nChannelId);

    void StopEvent(const string &strEventName, bool bImmediate = false);

    void GeteventParameter(const string &strEventName, const string% strEventParameter, float* parameter);

    void SetEventParameter(const string &strEventName, const string &strParameterName, float fValue);

    void StopAllChannels();

    void SetChannel3dPosition(int nChannelId, const Vector3 &vPosition);

    void SetChannelvolume(int nChannelId, float fVolumedB);

    bool IsPlaying(int nChannelId) const;

    bool IsEventPlaying(const string &strEventName) const;

    float dbToVolume(float db);

    float VolumeTodb(float volume);

    FMOD_VECTOR VectorToFmod(const Vector &vPosition);
   
    
private:

     


};





#endif // _FMODWRAPPER_H__
