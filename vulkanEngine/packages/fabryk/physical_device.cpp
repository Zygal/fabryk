#include "headers.h"

#ifndef INSTANCE
#define INSTANCE
#include "instance.cpp"
#endif

struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily = {};

	bool isComplete() {
		return graphicsFamily.has_value();
	}
};

class Fabryk_Physical_Device {
public:

	void init(Fabryk_Instance * f_instance) {
		instance = f_instance->getVkInstance();

		getPhysicalDevices();
		physicalDevice = getBestDevice();
		
	}

	VkPhysicalDevice getVkPhysicalDevice() {
		return physicalDevice;
	}

	QueueFamilyIndices getQueueFamilyIndices() {
		return indices;
	}

private:
	VkInstance instance;
	VkPhysicalDevice physicalDevice;
	std::vector<VkPhysicalDevice> availableDevices;

	QueueFamilyIndices indices;

	void getPhysicalDevices() {
		uint32_t deviceCount;
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

		if (deviceCount == 0) {
			throw std::runtime_error("Failed to find GPUs with Vulkan support");
		}

		availableDevices.resize(deviceCount);

		vkEnumeratePhysicalDevices(instance, &deviceCount, availableDevices.data());
	}

	VkPhysicalDevice getBestDevice() {
		uint32_t bestSuitability = 0;
		uint32_t currSuitability = 0;
		VkPhysicalDevice bestDevice;
		QueueFamilyIndices bestIndices;
		for (const auto& device : availableDevices) {
			currSuitability = deviceSuitability(device);
			if (currSuitability > bestSuitability) {
				bestIndices = indices;
				bestSuitability = currSuitability;
				bestDevice = device;
			}
		}
		indices = bestIndices;
		return bestDevice;
	}

	uint32_t deviceSuitability(VkPhysicalDevice device) {
		uint32_t suitability = 0;
		VkPhysicalDeviceProperties deviceProperties;
		VkPhysicalDeviceFeatures deviceFeatures;

		vkGetPhysicalDeviceProperties(device, &deviceProperties);
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			suitability += 50;
		}
		checkQueueFamilies(device);
		if (indices.isComplete()) {
			suitability += 50;
		}
		//Add more Suitability properties here
		/*
		*/
		return suitability;
	}

	void checkQueueFamilies(VkPhysicalDevice physicalDevice) {
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

		int i = 0;
		for (const auto& queueFamily : queueFamilies) {
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i;
			}

			if (indices.isComplete()) {
				break;
			}

			i++;
		}
	}
};