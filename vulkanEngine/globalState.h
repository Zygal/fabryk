#ifndef _GLOBALSTATE_H__
#define _GLOBALSTATE_H__

#include "singleton.h"

// Set up the inheritance for the global state, not pure virtual
class GlobalStore : public Singleton<GlobalStore>{

public:
    GlobalStore();
    virtual ~GlobalStore();
    GlobalStore* GSInstance();

};


#endif // _GLOBALSTATE_H__
